import sys

import grammar_io

def remove_rules_with_symbols_not_in(G, symbols):
    for lhs, rhss in G.copy().items():
        if lhs not in symbols:
            del G[lhs]
        else:
            for rhs in rhss.copy():
                if any(s not in symbols for s in rhs):
                    rhss.remove(rhs)

G = grammar_io.read(sys.stdin)

print('Original:')
grammar_io.write(G, sys.stdout)

T = {s for lhs, rhss in G.items() for rhs in rhss for s in rhs} - set(G)

N = set()
while True:
    N2 = N | {lhs for lhs, rhss in G.items()
                  if any(all(s in (N | T) for s in rhs) for rhs in rhss)}
    if N2 == N:
        break
    N = N2

remove_rules_with_symbols_not_in(G, T | N)

V = {'S'}
while True:
    V2 = V | {s for lhs, rhss in G.items()
                if lhs in V
                for rhs in rhss
                for s in rhs}
    if V2 == V:
        break
    V = V2

remove_rules_with_symbols_not_in(G, V)

print('Without useless symbols:')
grammar_io.write(G, sys.stdout)
